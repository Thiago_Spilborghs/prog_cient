#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Autor: Thiago Spilborghs Bueno Meyer
Criado em: 18 de Junho de 2019
Produto: Node em Programacao Orientada a Objeto
'''

class Node():

	'''
	Classe Node
	Implementação de um nó em python
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Alteração valor do nó (setvalor)
	-- Alteração do próximo valor (setprox)
	-- Retorno do valor do nó (getvalor)
	-- Retorno do valor do próximo nó (getprox)
	-- Destrutor do nó (__del__)

	Valores da Classe Node:
	-- Um valor para o nó (self.valor)
	-- Um valor para o próximo nó (self.proximo)
	'''

	def __init__(self,valor=None,prox=None,cost=None):
		self.valor = valor
		self.proximo = prox
		self.cost = cost

	def setvalor(self,valor):
		self.valor = valor

	def setcost(self,cost):
		self.cost = cost

	def setprox(self,valor):
		self.proximo = valor

	def getvalor(self):
		return self.valor

	def getprox(self):
		return self.proximo

	def getcost(self):
		return self.cost
		
	def __del__(self):
		self.valor = None
		self.proximo = None
		self.cost = None
		#print "Nó foi destruido"
