#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Autor: Thiago Spilborghs Bueno Meyer
Criado em: 11 de Junho de 2019
Produto: Pilha e Fila em Programacao Orientada a Objeto
'''

class Pilha():
	
	'''
	Classe Pilha
	Implementação da Estrutura de Dados Pilha
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Retirada de uma valor do topo da Pilha (pop)
	-- Inserção de um valor no topo da Pilha (push)
	-- Apresentação do valor do topo da Pilha (top)
	-- Retorno do valor da Pilha (ret)
	-- Atualização do valor da Pilha (update)
	-- Destrutor da Pilha (__del__)

	Valores da Classe Pilha:
	-- Um vetor para representação de uma Pilha (self.pilha)
	'''
	
	def __init__(self):
		self.pilha = []

	def pop(self):
		if self.IsNotEmpty():	
			A = self.ret()
			B = []
			c = len(A) - 1
			if c == 0:
				self.update(B)
				return A[c]
			else:	
				for i in range(0,c):
					B.append(i)
				self.update(B)
				return A[c]
		return None

	def IsNotEmpty(self):
		if len(self.ret()) == 0:
			return False
		else:
			return True

	def push(self,valor):
		A = self.ret()
		A.append(valor)
		self.update(A)

	def top(self):
		A = self.ret()
		pos = len(A) -1
		return A[pos]

	def ret(self):
		return self.pilha

	def update(self,valor):
		self.pilha = valor

	def __del__(self):
		self.update(None)
		print "Pilha foi destruida"

class Fila():

	'''
	Classe Fila
	Implementação da Estrutura de Dados Fila
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Retirada de uma valor do início da Fila (pop)
	-- Inserção de um valor no final da Fila (push)
	-- Apresentação do valor do início da Fila (first)
	-- Retorno do valor da Fila (ret)
	-- Atualização do valor da Fila (update)
	-- Destrutor da Fila (__del__)

	Valores da Classe Fila:
	-- Um vetor para representação de uma Fila (self.fila)
	'''

	def __init__(self):
		self.fila = []

	def push(self,valor):
		A = self.ret()
		A.append(valor)
		self.update(A)

	def pop(self):
		if self.IsNotEmpty():
			A = self.ret()
			B = []
			for i in range(1,len(A)):
				B.append(A[i])
			self.update(B)
			return A[0]
			
		return None

	def first(self):
		A = self.ret()
		return A[0]

	def IsNotEmpty(self):
		if len(self.ret()) == 0:
			return False
		else:
			return True

	def ret(self):
		return self.fila

	def update(self,valor):
		self.fila = valor

	def __del__(self):
		self.update(None)
		print "Fila foi destruida"
