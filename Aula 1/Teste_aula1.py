'''
TESTE das classes Pilha e Fila
'''

from collections import deque
from aula1_estatica import Pilha, Fila

import random as r
import time

if __name__ == '__main__':
	V_time_Pilha = []
	V_time_Fila = []
	V_time_deque_fila = []
	V_time_deque_pilha = []
	for i in range(0,30):
		size = r.randint(5,20)
		# Inicio teste pilha implementada
		inicio = time.time()
		Minha_Pilha = Pilha(size)
		for i in range(0,size):
			valor = r.randint(0,100)
			Minha_Pilha.push(valor)
		for i in range(0,size):
			a = Minha_Pilha.pop()
		fim = time.time() - inicio
		V_time_Pilha.append(fim)
		# Inicio teste pilha collection
		inicio = time.time()
		coll_Pilha = deque()
		for i in range(0,size):
			valor = r.randint(0,100)
			coll_Pilha.append(valor)
		for i in range(0,size):
			a = coll_Pilha.pop()
		fim = time.time() - inicio
		V_time_deque_pilha.append(fim)
		# Inicio teste fila implementada
		inicio = time.time()
		Minha_Fila = Fila(size)
		for i in range(0,size):
			valor = r.randint(0,100)
			Minha_Fila.push(valor)
		for i in range(0,size):
			a = Minha_Fila.pop()
		fim = time.time() - inicio
		V_time_Fila.append(fim)
		# Inicio teste fila collection
		inicio = time.time()
		coll_Fila = deque()
		for i in range(0,size):
			valor = r.randint(0,100)
			coll_Fila.append(valor)
		for i in range(0,size):
			a = coll_Fila.popleft()
		fim = time.time() - inicio
		V_time_deque_fila.append(fim)

	file = open("tempo_fila_implem.txt",'w')
	for i in V_time_Fila:
		file.write(str(i))
		file.write('\n')
	file.close()

	file = open("tempo_fila_deque.txt",'w')
	for i in V_time_deque_fila:
		file.write(str(i))
		file.write('\n')
	file.close()

	file = open("tempo_pilha_implem.txt",'w')
	for i in V_time_Pilha:
		file.write(str(i))
		file.write('\n')
	file.close()

	file = open("tempo_pilha_deque.txt",'w')
	for i in V_time_deque_pilha:
		file.write(str(i))
		file.write('\n')
	file.close()
