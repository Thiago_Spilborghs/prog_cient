#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Autor: Thiago Spilborghs Bueno Meyer
Criado em: 11 de Junho de 2019
Produto: Pilha e Fila em Programacao Orientada a Objeto
'''

class Pilha():
	
	'''
	Classe Pilha
	Implementação da Estrutura de Dados Pilha
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Retirada de uma valor do topo da Pilha (pop)
	-- Inserção de um valor no topo da Pilha (push)
	-- Apresentação do valor do topo da Pilha (top)
	-- Retorno do valor da Pilha (ret)
	-- Atualização do valor da Pilha (update)
	-- Destrutor da Pilha (__del__)

	Valores da Classe Pilha:
	-- Um vetor para representação de uma Pilha (self.pilha)
	'''
	
	def __init__(self,tam=0):
		self.pilha = []
		self.size = tam
		self.top = -1

	def pop(self):
		if self.IsNotEmpty():	
			A = self.ret()
			B = []
			c = len(A) - 1
			if c == 0:
				self.update(B)
				return A[c]
			else:	
				for i in range(0,c):
					B.append(i)
				self.update(B)
				return A[c]
		return None

	def IsNotEmpty(self):
		if len(self.ret()) == 0:
			return False
		else:
			return True

	def push(self,valor):
		A = self.ret()
		c = len(A)
		if c == self.SizeOfQueue():
			print "Pilha Cheia, valor não adicionado"
		else:
			A.append(valor)
			self.update(A)
			self.top += 1

	def Top(self):
		A = self.ret()
		if self.top == -1:
			return None
		return A[self.top]

	def ret(self):
		return self.pilha

	def SizeOfQueue(self):
		return self.size

	def update(self,valor):
		self.pilha = valor

	def __del__(self):
		self.update(None)
		print "Pilha foi destruída"

class Fila():

	'''
	Classe Fila
	Implementação da Estrutura de Dados Fila
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Retirada de uma valor do início da Fila (pop)
	-- Inserção de um valor no final da Fila (push)
	-- Apresentação do valor do início da Fila (first)
	-- Retorno do valor da Fila (ret)
	-- Atualização do valor da Fila (update)
	-- Destrutor da Fila (__del__)

	Valores da Classe Fila:
	-- Um vetor para representação de uma Fila (self.fila)
	'''

	def __init__(self,tam=0):
		self.fila = []
		self.size = tam

	def push(self,valor):
		A = self.ret()
		c = len(A)
		if c == self.SizeOfQueue():
			print "Fila Cheia, valor não adicionado"
		else:
			A.append(valor)
			self.update(A)

	def pop(self):
		if self.IsNotEmpty():
			A = self.ret()
			B = []
			for i in range(1,len(A)):
				B.append(A[i])
			self.update(B)
			return A[0]

		return None

	def First(self):
		A = self.ret()
		return A[0]

	def IsNotEmpty(self):
		if len(self.ret()) == 0:
			return False
		else:
			return True

	def SizeOfQueue(self):
		return self.size

	def ret(self):
		return self.fila

	def update(self,valor):
		self.fila = valor

	def __del__(self):
		self.update(None)
		print "Fila foi destruida"
