#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Autor: Thiago Spilborghs Bueno Meyer
Criado em: 11 de Junho de 2019
Produto: Fila em Programacao Orientada a Objeto utilizando Herança da classe ListaLigada
'''
from Lista_Ligada import ListaLigada

class Fila(ListaLigada):

	'''
	Classe Fila
	Implementação da Estrutura de Dados Fila
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Retirada de uma valor do início da Fila (pop)
	-- Inserção de um valor no final da Fila (push)
	-- Apresentação do valor do início da Fila (first)
	-- Destrutor da Fila (__del__)

	'''

	def Push(self,valor):
		self.Insert_Back(valor)

	def Pop(self):
		valor = self.Delete_front()
		return valor

	def first(self):
		return self.Getinicio().getvalor()

	def __del__(self):
		self.DELALL()
		print "Fila foi destruida"
