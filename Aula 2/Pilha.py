#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Autor: Thiago Spilborghs Bueno Meyer
Criado em: 11 de Junho de 2019
Produto: Pilha e Fila em Programacao Orientada a Objeto utilizando Herança da classe ListaLigada
'''
from Lista_Ligada import ListaLigada

class Pilha(ListaLigada):
	
	'''
	Classe Pilha
	Implementação da Estrutura de Dados Pilha
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Retirada de uma valor do topo da Pilha (pop)
	-- Inserção de um valor no topo da Pilha (push)
	-- Apresentação do valor do topo da Pilha (top)
	-- Destrutor da Pilha (__del__)
	'''

	def Push(self,valor):
		self.Insert_Back(valor)

	def Pop(self):
		try:
			fim = self.Delete_back()
			return fim
		except IndexError:
			return None

	def top(self):
		return self.Getfim().getvalor()

	def __del__(self):
		self.DELALL()
		print "Pilha foi destruida"
