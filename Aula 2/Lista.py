#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Autor: Thiago Spilborghs Bueno Meyer
Criado em: 18 de Junho de 2019
Produto: Lista em Programacao Orientada a Objeto utilizando Herança da classe ListaLigada
Descrição:
	Criar listas com funções para auxiliar sua manipulação 
	e utilização em códigos futuros.

'''
from Lista_Ligada import ListaLigada

class Lista(ListaLigada):

	'''
	Classe Node
	Implementação de uma Lista em python
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Retirada de uma valor do topo da Lista (Pop)
	-- Inserção de um valor no topo da Lista (Push)
	-- Apresentação do valor do topo da Lista (Last)
	-- Apresentação do valor do inicio da Lista (First)
	-- Retorna o valor Máximo da Lista (max)
	-- Remove um valor específico da Lista (Remove)
	-- Imprime a Lista (ret)
	-- Destrutor do nó (__del__)

	'''

	def Push(self,valor):
		self.Insert_Back(valor)

	def Pop(self):
		try:
			fim = self.Delete_back()
			return fim
		except IndexError:
			return None

	def max(self):
		no = self.inicio
		maximo = no.getvalor()
		while no != None:
			if no.getvalor() > maximo:
				maximo = no.getvalor()
			no = no.getprox()
		return maximo

	def First(self):
		return self.Getinicio().getvalor()

	def Remove(self, valor):
		try:
			no = self.Find(valor)
			if no == self.inicio:
				self.inicio = no.getprox()
				del no
				return True
			else:
				no_it = self.inicio
				while no_it != None:
					if no_it.getprox() == no:
						no_it.setprox(no.getprox())
						del no
						return True
			return False
		except ValueError:
			print "Error, value not found"
			return False


	def Last(self):
		return self.Getfim().getvalor()

	def ret(self):
		self.Print()

	def __del__(self):
		self.DELALL()
		print "Lista foi destruida"
