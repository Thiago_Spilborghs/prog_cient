'''
TESTE das classes Pilha, Fila e Lista implementadas com Herança (Lista Ligada).
Teste:
	Utilizando 30 testes e gerando um valor aleatório entre 100 e 1000 para o tamanho,
	será inserido valores aleatórios nas estruturas de dados analisadas e em seguida
	os valores são removidos, um a um.
'''

from collections import deque
from Fila import Fila
from Pilha import Pilha
from Lista import Lista


import random as r
import time

if __name__ == '__main__':
	V_FILA = []
	V_PILHA = []
	V_LISTA = []
	V_DEQUE_F = []
	V_DEQUE_P = []
	V_list_py = []
	for i in range(0,30):
		size = r.randint(100,1000)
		# Inicio do Teste com a classe Fila
		inicio = time.time()
		fila = Fila()
		for j in range(0,size):
			valor = r.randint(0,100)
			fila.Push(valor)
		for j in range(0,size):
			fila.Pop()
		fim = time.time() - inicio
		V_FILA.append(fim)
		# Inicio do Teste com a Fila usando a classe deque
		inicio = time.time()
		fila = deque()
		for j in range(0,size):
			valor = r.randint(0,100)
			fila.append(valor)
		for j in range(0,size):
			fila.popleft()
		fim = time.time() - inicio
		V_DEQUE_F.append(fim)
		# Inicio do Teste com a classe Pilha
		inicio = time.time()
		fila = Pilha()
		for j in range(0,size):
			valor = r.randint(0,100)
			fila.Push(valor)
		for j in range(0,size):
			fila.Pop()
		fim = time.time() - inicio
		V_PILHA.append(fim)
		# Inicio do Teste com a Fila usando a classe deque
		inicio = time.time()
		fila = deque()
		for j in range(0,size):
			valor = r.randint(0,100)
			fila.append(valor)
		for j in range(0,size):
			fila.pop()
		fim = time.time() - inicio
		V_DEQUE_P.append(fim)
		# Inicio do Teste com a classe Lista
		inicio = time.time()
		fila = Lista()
		for j in range(0,size):
			valor = r.randint(0,100)
			fila.Push(valor)
		for j in range(0,size):
			fila.Pop()
		fim = time.time() - inicio
		V_LISTA.append(fim)
		# Inicio do Teste com Lista utilizando o list do python
		inicio = time.time()
		fila = []
		for j in range(0,size):
			valor = r.randint(0,100)
			fila.append(valor)
		for j in range(0,size):
			fila.pop()
		fim = time.time() - inicio
		V_list_py.append(fim)

	file = open("MYFILA.csv",'w')
	for i in V_FILA:
		a = str(i)
		a = a.replace(".",",")
		file.write(a)
		file.write("\n")
	file.close()

	file = open("MYPILHA.csv",'w')
	for i in V_PILHA:
		a = str(i)
		a = a.replace(".",",")
		file.write(a)
		file.write("\n")
	file.close()

	file = open("MYLISTA.csv",'w')
	for i in V_LISTA:
		a = str(i)
		a = a.replace(".",",")
		file.write(a)
		file.write("\n")
	file.close()

	file = open("DEQUEFILA.csv",'w')
	for i in V_DEQUE_F:
		a = str(i)
		a = a.replace(".",",")
		file.write(a)
		file.write("\n")
	file.close()

	file = open("DEQUEPILHA.csv",'w')
	for i in V_DEQUE_P:
		a = str(i)
		a = a.replace(".",",")
		file.write(a)
		file.write("\n")
	file.close()

	file = open("LISTPYTHON.csv",'w')
	for i in V_list_py:
		a = str(i)
		a = a.replace(".",",")
		file.write(a)
		file.write("\n")
	file.close()
