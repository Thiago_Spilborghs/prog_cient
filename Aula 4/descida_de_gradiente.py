# -*- coding: utf-8 -*- 

#Autor: Thiago Spilborghs Bueno Meyer
#Função Descida de Gradiente que realiza uma iteração para achar o mínimo de uma função
#Entrada: 
'''
	f -> Função a qual é desejado o mínimo
	df -> Derivada da função inserida
	x -> Chute inicial
	tolerance -> Tolerância para o mínimo (default = 0.00001)
	Beta -> Taxa de aprendizado (default = 0.1)
'''
#Saída:
#	x -> Raíz da função dada ou, em caso de erro, valor próximo à raiz da função
def Descida_de_Gradiente(f, df, x, tolerance=0.00001,Beta=0.1):
	i = 10000 # Limite de iterações
	while True:
		si = df(x) #derivada da função
		x1 = x - Beta*si #cálculo do novo valor
		t = abs(x1 - x) 
		if t < tolerance: #verifica se condição satisfaz o mínimo da função
			return x
		if i < 0: #Verifica se chegou no limite de iterações
			print "Could not find an answer"
			return x #Retorna o valor encontrado até o momento
		i = i - 1
		x = x1
	return x

def df1(x): #Derivada da função 1
	return 2*x

def f1(x=None):  #Função 1
	if x == None:
		return "x^2"
	else:
		return x**2

def f2(x=None): #Função 2
	if x == None:
		return "x^3 - 2*x^2 +2"
	else:
		return x**3 - 2*x**2 +2

def df2(x): #Derivada da função 1
	return ((3*x**2) - (4*x))

def main():
	X0 = 2 #Chute inicial

	x = Descida_de_Gradiente(f1,df1,X0)
	print "Função: "+str(f1())
	print "Mínimo da Função: "+str(x)
	print "Valor utilizando mínimo: "+str(f1(x))

	print "\n\n"

	x = Descida_de_Gradiente(f2,df2,X0)
	print "Função: "+str(f2())
	print "Mínimo da Função: "+str(x)
	print "Valor utilizando mínimo: "+str(f2(x))

if __name__ == '__main__':
	main()