'''
Teste aula3.py DFS, BFS, A* e HillClimbing
'''
from Astar import Astar
from HC import HillClimbing
from BFS import BFS
from DFS import DFS

if __name__ == '__main__':

	wanted = [0,1,2,3,4,5,6,7,8]
	start = [1,0,2,3,4,5,6,7,8]

	print "inicio = "+str(start)
	
	vis,custo = HillClimbing(start,wanted)
	if vis == None:
		print "Sorry, but it was not possible to find an answer"
	else:
		print "HC: "+str(vis)
		print "cost: "+str(custo)

	vis,custo = BFS(start,wanted)
	if vis == None:
		print "Sorry, but it was not possible to find an answer"
	else:
		print "BFS: "+str(vis)
		print "Moves: "+str(custo)

	vis,custo = DFS(start,wanted)
	if vis == None:
		print "Sorry, but it was not possible to find an answer"
	else:
		print "DFS: "+str(vis)
		print "Moves: "+str(custo)

	vis,custo = Astar(start,wanted)
	if vis == None:
		print "Sorry, but it was not possible to find an answer"
	else:
		print "A*: "+str(vis)
		print "cost: "+str(custo)



