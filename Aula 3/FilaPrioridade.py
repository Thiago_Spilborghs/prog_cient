#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Autor: Thiago Spilborghs Bueno Meyer
Criado em: 25 de Junho de 2019
Produto: Fila com prioridade em Programacao Orientada a Objeto utilizando Herança da classe ListaLigada
'''

from Lista_Ligada import ListaLigada
from node import Node

class FilaPrioridade(ListaLigada):

	'''
	Classe Fila
	Implementação da Estrutura de Dados Fila
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Retirada de uma valor do início da Fila (pop)
	-- Inserção de um valor no final da Fila (push)
	-- Apresentação do valor do início da Fila (first)
	-- Destrutor da Fila (__del__)

	'''

	def Push(self,valor,cost):
		self.Insert_Back(valor,cost)

	def Pop(self):
		try:
			no = self.inicio
			valor = no.getcost()
			escolhido = no
			while no != self.fim:
				no=no.getprox()
				if no.getcost() < valor:
					valor = no.getcost()
					escolhido = no
			if self.Tamanho() == 1:
				valor = escolhido.getvalor()
				cost = escolhido.getcost()
				del escolhido
				self.inicio = None
				self.fim = None
				return (valor,cost)
			else:
				no_aux = self.inicio
				while no_aux.getprox() != escolhido:
					no_aux = no_aux.getprox()
				no_aux.setprox(escolhido.getprox())
				if escolhido == self.fim:
					self.fim = no_aux
				valor = escolhido.getvalor()
				cost = escolhido.getcost()
				del escolhido
				return (valor,cost)
		
		except IndexError as e:
			print "Error while removing item: "+str(e)
			return None
		except AttributeError as e:
			print "First Node is None"
			return None


	def first(self):
		return self.Getinicio().getvalor()

	def ret(self):
		Vetor = []
		if not self.Vazio():
			no = self.inicio
			while no!=None:
				Vetor.append(no.getvalor())
				no = no.getprox()
			return Vetor
		return Vetor

	def __del__(self):
		self.DELALL()
		#print "Fila foi destruida"
