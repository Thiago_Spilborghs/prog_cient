from fila import Fila
from pilha import Pilha
from util import expand

def DFS(inicio,fim):
    visitado = Fila()
    fila = Pilha()
    fila.Push(inicio)
    movimentos = Pilha()
    movs = []
    #visitado = list()
    #fila = list()
    #fila.append(inicio)
    while len(fila.ret()) != 0:
        vertice = fila.Pop()
        #print "vertice "+str(vertice)
        #print vertice.getvalor()
        #vertice = fila.pop(0)
        if vertice == None:
            break
        elif vertice not in visitado.ret():
        #elif vertice not in visitado:
            visitado.Push(vertice)
            if vertice != inicio:
                movimentos.Push(movs.pop())
            #print vertice
            #visitado.append(vertice)
            if vertice == fim:
                return (visitado.ret(),movimentos.ret())
                #return visitado
            vizinhos, movtemp = expand(vertice)
            for i in movtemp:
                movs.append(i)
            #vizinhos.Print()
            for n in vizinhos.ret():
            #for n in vizinhos.ret():
                #v = visitado.ret()
                if n not in visitado.ret():
                #if n not in visitado:
                    fila.Push(n)
                    #fila.append(n)

    return None