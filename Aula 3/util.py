from fila import Fila
from node import Node

def dist(state):
    dist = 0
    for i in state:
        dist += abs(pow(i-state.index(i),2))
    return dist

def expand(node):
    Neighbours = Fila()
    movements = []
    node1 = node
    node2 = node
    node3 = node
    node4 = node
    mv1 = move(node1, 1)
    mv2 = move(node2, 2)
    mv3 = move(node3, 3)
    mv4 = move(node4, 4)
    if mv1 != None:
        Neighbours.Push(mv1)
        movements.append("Up")
    if mv2 != None:
        Neighbours.Push(mv2)
        movements.append("Down")
    if mv3 != None:
        Neighbours.Push(mv3)
        movements.append("Left")
    if mv4 != None:
        Neighbours.Push(mv4)
        movements.append("Right")
        

    return (Neighbours, movements)

def move(node, mov):
    new_node = []
    vet_temp = node
    index = vet_temp.index(0)
    for i in vet_temp:
        new_node.append(i)
    if mov == 1:  # Up
        if index not in range(0, 3):
            temp = new_node[index]
            new_node[index] = new_node[int(index-3)]
            new_node[int(index-3)]=temp
            return new_node
        else:
            return None

    if mov == 2:  # Down
        if index not in range(6, 9):
            temp = new_node[index + 3]
            new_node[index + 3] = new_node[index]
            new_node[index] = temp
            return new_node
        else:
            return None

    if mov == 3:  # Left
        if index != 0 and index != 3 and index != 6:
            temp = new_node[index - 1]
            new_node[index - 1] = new_node[index]
            new_node[index] = temp
            return new_node
        else:
            return None

    if mov == 4:  # Right
        if index != 2 and index!= 5 and index != 8:
            temp = new_node[index + 1]
            new_node[index + 1] = new_node[index]
            new_node[index] = temp
            return new_node
            
        else:
            return None
