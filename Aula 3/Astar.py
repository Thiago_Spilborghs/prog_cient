from FilaPrioridade import FilaPrioridade
from util import expand, dist

def Astar(start, goal):
    frontier = FilaPrioridade()
    frontier.Push(start,0)
    came_from = {}
    cost_so_far = {}
    came_from[str(start)] = None
    cost_so_far[str(start)] = 0
    
    while not frontier.Vazio():
        current, cost = frontier.Pop()
        
        if current == goal:
            break

        vizinhos, movtemp = expand(current)

        for prox in vizinhos.ret():
            new_cost = cost_so_far[str(current)] + 1
            if str(prox) not in cost_so_far or new_cost < cost_so_far[str(prox)]:
                cost_so_far[str(prox)] = new_cost
                priority = new_cost + dist(prox)
                frontier.Push(prox, priority)
                came_from[str(prox)] = current
    
    return came_from, cost_so_far