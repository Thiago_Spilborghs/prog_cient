#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Autor: Thiago Spilborghs Bueno Meyer
Criado em: 18 de Junho de 2019
Produto: Lista Ligada em Programacao Orientada a Objeto
'''
from node import Node

class ListaLigada():

	'''
	Classe ListaLigada
	Implementação da Estrutura de Dados Lista Ligada
	
	Com funções de:
	-- Inicialização de variáveis (__init__)
	-- Inserção de um valor no final da Lista (Insert_Front)
	-- Inserção de um valor no inicio da Lista (Insert_Back)
	-- Retorna o tamanho da Lista (Tamanho)
	-- Retorna o nó requisitado da Lista (Find)
	-- Apaga um nó do inicio da Lista (Delete_front)
	-- Apaga um nó do final da Lista (Delete_back)
	-- Imprime a Lista Ligada (print)
	-- Retorna o valor do nó inicial (Getinicio)
	-- Retorna o valor do nó final (Getfim)
	-- Apaga a Lista (DELALL)
	-- verifica se a Lista está vazia (vazio)
	-- Destrutor da Fila (__del__)

	Valores da Classe Lista:
	-- Um valor do inicio da Fila (self.inicio)
	-- Um valor do fim da Fila (self.fim)
	'''

	def __init__(self,inicio=None):
		self.inicio = inicio
		self.fim = None 

	def Insert_Front(self,valor):
		try:
			if self.inicio != None:
				no = Node(valor,None)
				no.setprox(self.inicio)
				self.inicio = no
				return False
			else:
				no = Node(valor,None)
				self.inicio = no
				self.fim = no
			return True
		except MemoryError as e:
			print "Error While inserting value: "+str(e)
			return False


	def Insert_Back(self,valor,cost=None):
		try:
			if cost != None:
				novo_no = Node(valor,None,cost)
			else:
				novo_no = Node(valor,None)
			if self.Vazio(): 
				self.inicio = novo_no
				self.fim = self.inicio
			elif self.Tamanho == 1:
				self.inicio.setprox(novo_no)
				self.fim = novo_no
			else:
				ultimo = self.inicio
				while ultimo.getprox() != None:
					ultimo = ultimo.getprox()
				ultimo.setprox(novo_no)
				self.fim = novo_no
			return True
		except MemoryError as e:
			print "Error while inserting value: "+str(e)
			return False

	def Tamanho(self):
		if self.Vazio():
			return 0
		else:
			cont = 0
			no = self.inicio
			while no != None:
				no = no.getprox()
				cont = cont + 1

			return cont

	def Find(self,valor = None):
		if valor == None:
			return self.inicio
		if self.Vazio():
			raise ValueError
			return None
		else:
			no = self.inicio
			while no != None:
				if no.getvalor() == valor:
					return no
				no = no.getprox()
			raise ValueError
			return None

	def Delete_front(self):
		try:
			if self.inicio == self.fim:
				valor = self.inicio.getvalor()
				self.inicio = None
				self.fim = None
				return valor
			else:
				no = self.inicio
				self.inicio = no.getprox()
				valor = no.getvalor()
				del no
				return valor
		except IndexError as e:
			print "Error while removing item: "+str(e)
			return None
		except AttributeError as e:
			print "First Node is None"
			return None

	def Delete_back(self):
		try:
			if self.Tamanho() == 1:
				no = self.fim
				valor = no.getvalor()
				del no
				self.inicio = None
				self.fim = None
				return valor
			else:
				no = self.inicio
				while no != None:
					if no.getprox() == self.fim:
						rm_no = no.getprox()
						valor = self.fim.getvalor()
						no.setprox(None)
						del rm_no
						self.fim = no
						return valor
					no = no.getprox()
				return None
		except IndexError as e:
			print "Error while removing from list: "+str(e)
			return None

	def Print(self):
		no = self.inicio
		print ("["),
		while no != None:
			if no.getprox() == None:
				print (str(no.getvalor())),
			else:	
				print (str(no.getvalor()) + ","),
			no = no.getprox()
		print("]")

	def Getinicio(self):
		return self.inicio

	def Getfim(self):
		return self.fim

	def DELALL(self):
		no = self.inicio
		self.inicio = None
		while no != None:
			rm_no = no
			no = no.getprox()
			del rm_no
		return True

	def Vazio(self):
		if self.inicio == None and self.fim == None:
			return True
		else:
			return False

	def __del__(self):
		self.DELALL()
		#print "Lista foi destruida"
