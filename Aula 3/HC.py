from FilaPrioridade import FilaPrioridade
from util import expand, dist

def HillClimbing(start, goal):
    frontier = FilaPrioridade()
    frontier.Push(start,dist(start))
    came_from = {}
    cost_so_far = {}
    came_from[str(start)] = None
    cost_so_far[str(start)] = 0
    anterior = dist(start) + 1
    Success = False
    
    while not frontier.Vazio():
        current, cost = frontier.Pop()
        if current == goal:
            Success = True
            break
        if cost < anterior:

            vizinhos, movtemp = expand(current)

            NewFrontier = FilaPrioridade()
            for prox in vizinhos.ret():
                new_cost = cost_so_far[str(current)] + 1
                if str(prox) not in cost_so_far:
                    cost_so_far[str(prox)] = new_cost
                    priority = dist(prox)
                    NewFrontier.Push(prox, priority)
                    came_from[str(prox)] = current
            frontier = NewFrontier
            anterior = cost
    if Success:
        return came_from, cost_so_far
    else:
        return None, cost_so_far