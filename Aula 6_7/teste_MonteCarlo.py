import Montecarlo
import numpy as np
from util import *
import time

def main():
	v = [(toroide,uniform_points),(toroide,random_points),(toroide,np.random.uniform),(f,uniform_points,0,1),(f,random_points,0,1),(f,np.random.uniform,0,1),(f2,uniform_points,0,1),(f2,random_points,0,1),(f2,np.random.uniform,0,1)]
	n = [10,100,1000,10000]
	for i in v:
		if i[0] == toroide:
			p1 = "toroide"
		elif i[0] == f:
			p1 = "funcao1"
		else:
			p1 = "funcao2"

		if i[1] == uniform_points:
			p2 = "uniform"

		elif i[1] == random_points:
			p2 = "random"

		else:
			p2 = "numpy_random_uniform"

		file = open(p1+p2+"_.txt",'w')
		file2 = open("time_"+p1+"_"+p2+"_.txt",'w')
		for cortes in n:
			file.write("valor ; erro ; N cortes")
			file.write('\n')
			file2.write("N cortes ; tempo (ms)")
			file2.write('\n')
			for j in range(0,30):
				inicio = time.time()
				val = None
				if len(i) == 5:
					val = Montecarlo.MonteCarlo(funcao=i[0],ger_ponto=i[1],x1=i[2],x2=i[3],cortes=cortes)
				else:
					val = Montecarlo.MonteCarlo(funcao=i[0],ger_ponto=i[1],cortes = cortes)
				fim = time.time() - inicio
				file.write(str(val[0])+" ; "+str(val[1])+" ; "+str(cortes))
				file.write("\n")
				file2.write(str(cortes)+" ; "+str(fim))
				file2.write('\n')
		file.close()
		file2.close()

if __name__ == '__main__':
	main()