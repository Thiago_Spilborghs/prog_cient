from decimal import Decimal
import random

'''
Criado por: Thiago Spilborghs Bueno Meyer
Descricao: 
	uniform_points: recebe um valor para inicio, um para fim e o número de pontos que devem ser gerados.
	Essa funcao separa pontos uniformemente separados e os coloca em uma lista.
	
	random_points: recebe um valor para inicio, um para fim e o número de pontos que devem ser gerados.
	Essa funcao separa pontos randomicamente e os coloca em uma lista ordenada.

	f: funcao 1 para teste

	f2: funcao 2 para teste

	toroide: funcao definicao para o toroide.

	OBS: Foi utilizado a biblioteca Decimal, para realizar a conta exata de divisão, devido ao float ser em base 2 e algumas contas aparecerem com lixo de memória.
	
'''
def uniform_points(inicio,fim,cortes):
	a = Decimal(inicio)
	pontos = []
	step = Decimal(fim - inicio)/Decimal(cortes)
	while a <= fim:
		pontos.append(float(a))
		a += step

	return pontos

def random_points(inicio,fim,cortes):
	pontos = []
	dif = (fim-inicio)
	for i in range(0,cortes):
		step = random.random()
		pontos.append(inicio+dif*step)

	return sorted(pontos)

def f(x):
	return 4/(1+x**2)

def f2(x):
	return (x+x**0.5)**0.5

def toroide(x,y,z):
	return x>1 and y>=-3 and ((z**2)+(((x**2+y**2)**0.5)-3)**2) <=1