import numpy as np
import matplotlib.pyplot as plt
from util import *

def MonteCarlo(funcao, ger_ponto = uniform_points, plot_var=False, x1=1,x2=4,y1=-3, y2=4,z1=-1,z2=1, cortes=10):
	integral = 0 #inicia o valor da initegral

	if funcao == toroide:
		#Gera os pontos aleatorios
		points1 = ger_ponto(x1,x2,cortes)
		points2 = ger_ponto(y1,y2,cortes)
		points3 = ger_ponto(z1,z2,cortes)
		PinTorus = 0
		for i,j,k in zip(points1,points2,points3):
			#verifica se os pontos estao dento do toroide
			if funcao(i,j,k):
				PinTorus += 1

		Pp = PinTorus / float(cortes) #Porcentagem pontos
		VolumeCubo = float(x2 - x1) * float(y2 - y1) * float(z2 - z1)
		Volume_Torus = (VolumeCubo * Pp)
		#calcula erro
		error = VolumeCubo * abs((Pp - Pp**2) / cortes)**0.5
		val = Volume_Torus
		return (val,error)

	else:
		#Gera os pontos aleatorios
		points1 = ger_ponto(x1,x2,cortes)
		erro_acum = 0
		for i in points1:
			#calcula a soma da integral
			integral += funcao(i)
			erro_acum += (funcao(i))**2

		#calculo de erro
		error = (x2-x1)*(((erro_acum/len(points1)) - (integral/len(points1))**2 )/(len(points1)))**0.5
		v = []
		for i in points1:
			v.append(funcao(i))
	
		if plot_var == True:
			#Plota o gráfico
			plt.plot(np.linspace(x1,x2,1000), funcao(np.linspace(x1,x2,1000)))
			plt.stem(points1, v)
			plt.show()
		val = integral/cortes
		return (val,error)
