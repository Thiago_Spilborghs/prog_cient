from mpi4py import MPI
import numpy as np
from Montecarlo import MonteCarlo
from util import *

if __name__ == '__main__':

    funcao=toroide #define a funcao como o toroide
    geracao_pontos=np.random.uniform # define o metodo de geracao de pontos
    x1=1
    x2=4
    y1=-3
    y2=4
    z1=-1
    z2=1
    cortes=1000 #define o numero de pontos a serem gerados
    comm = MPI.COMM_WORLD #criacao da comunicacao mpi
    nprocs = comm.Get_size() #recuperacao da quantidade de processos
    myrank = comm.Get_rank() # recebe  o numero do processo

    # passo para ser verificado de x1 ate x2, y1 ate y2.
    stepx = float(x2-x1)/float(nprocs)
    stepy = float(y2-y1)/float(nprocs)
    
    #calculo do volume do toroide
    mc = MonteCarlo(funcao,geracao_pontos,plot_var=False,x1=x1+(myrank*stepx),x2=x1+(myrank*stepx)+stepx,y1=y1+(myrank*stepy),y2=y1+(myrank*stepy)+stepy,z1=z1,z2=z2,cortes=cortes/nprocs)

    erro = None

    #volta todos os valores para o processo 0
    valor = comm.reduce(mc[0], root=0)
    erro = comm.reduce(mc[1], root=0)

    #imprime o valor verificado
    if myrank == 0:
        print "Valor = "+str(valor*nprocs)
        if erro:
            print "Erro : "+str(erro*nprocs)
