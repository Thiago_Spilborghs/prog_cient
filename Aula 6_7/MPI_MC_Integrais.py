from mpi4py import MPI
import numpy as np
from Montecarlo import MonteCarlo
from util import *

if __name__ == '__main__':

    funcao = f #funcao 1 definida em util.py
    funcao2 = f2 #funcao 2 definida em util.py
    a = 0 # valor inicial
    b = 1# valor final
    cortes = 1000 #numero de pontos a serem separados
    geracao_pontos = np.random.uniform #metodo de geracao de pontos
    comm = MPI.COMM_WORLD #criacao da comunicacao mpi
    nprocs = comm.Get_size() #recuperacao da quantidade de processos
    myrank = comm.Get_rank() # recebe  o numero do processo

    stepx = float(b-a)/float(nprocs) # passo para ser verificado de a ate b.

    #calcula o montecarlo utilizando partes diferente para cada processo
    mc = MonteCarlo(funcao,geracao_pontos,plot_var=False,x1=a+(myrank*stepx),x2=a+(myrank*stepx)+stepx,cortes=cortes/nprocs)
    
    #volta todos os valores para o processo 0
    valor = comm.reduce(mc[0], root=0)
    erro = comm.reduce(mc[1],root=0)

    if myrank == 0:
        #imprime o valor verificado
        print "Valor funcao1 = "+str(valor*nprocs)
        print "Erro funcao1 = "+str(erro*nprocs)

    #repete o processo para a funcao 2
    mc = MonteCarlo(funcao2,geracao_pontos,plot_var=False,x1=a+(myrank*stepx),x2=a+(myrank*stepx)+stepx,cortes=cortes/nprocs)
    
    valor = comm.reduce(mc[0], root=0)
    erro = comm.reduce(mc[1],root=0)


    if myrank == 0:
        print "Valor funcao2 = "+str(valor*nprocs)
        print "Erro funcao2 = "+str(erro*nprocs)
