import Montecarlo
from mpi4py import MPI
import numpy as np
from util import *
import time

if __name__ == '__main__':
	comm = MPI.COMM_WORLD
	nprocs = comm.Get_size()
	myrank = comm.Get_rank()
	vet_cortes = [10,100,1000,10000]
	vet_FC_FG = [(toroide,random_points),(toroide,uniform_points),(toroide,np.random.uniform),(f,random_points),(f,uniform_points),(f,np.random.uniform),(f2,random_points),(f2,uniform_points),(f2,np.random.uniform)]
	file1 = {}
	file2 = {}
	for i in vet_FC_FG:
		if i[0] == toroide:
			p1 = "MPI_toroide"
		elif i[0] == f:
			p1 = "MPI_funcao1"
		else:
			p1 = "MPI_funcao2"

		if i[1] == uniform_points:
			p2 = "uniform"

		elif i[1] == random_points:
			p2 = "random"

		else:
			p2 = "numpy_random_uniform"

		#file = open(p1+p2+"_.txt",'w')
		#file2 = open("time_"+p1+"_"+p2+"_.txt",'w')
		time_vet = []
		valor_vet = []
		for cortes in vet_cortes:
			for j in range(0,30):
				inicio = time.time()
				
				if i[0] != toroide:
					a = 0
					b = 1
					stepx = float(b-a)/float(nprocs)
					mc = Montecarlo.MonteCarlo(i[0],i[1],plot_var=False,x1=a+(myrank*stepx),x2=a+(myrank*stepx)+stepx,cortes=cortes/nprocs)
				else:
					x1=1
					x2=4
					y1=-3
					y2=4
					z1 = -1
					z2 = 1

					stepx = float(x2-x1)/float(nprocs)
					stepy = float(y2-y1)/float(nprocs)

					mc = Montecarlo.MonteCarlo(i[0],i[1],plot_var=False,x1=x1+(myrank*stepx),x2=x1+(myrank*stepx)+stepx,y1=y1+(myrank*stepy),y2=y1+(myrank*stepy)+stepy,z1=z1,z2=z2,cortes=cortes/nprocs)
								
				valor = comm.reduce(mc[0], root=0)
				erro = comm.reduce(mc[1],root=0)

				if myrank == 0:
					valor = valor*nprocs
					erro = erro*nprocs
					fim = time.time() - inicio
					time_vet.append((cortes,fim))
					valor_vet.append((valor,erro,cortes))
		if myrank == 0:
			file1[str(p1)+"_"+str(p2)] = valor_vet
			file2["time_"+str(p1)+"_"+str(p2)] = time_vet

	if myrank == 0:
		for name in file1:
			arq_valor = open(str(name)+".txt","w")
			for j in file1[name]:
				arq_valor.write(str(j[0])+" ; "+str(j[1])+" ; "+str(j[2]))
				arq_valor.write("\n")
			arq_valor.close()

		for name in file2:
			arq_tempo = open(str(name)+".txt","w")
			for j in file2[name]:
				arq_tempo.write(str(j[0])+" ; "+str(j[1]))
				arq_tempo.write("\n")
			arq_tempo.close()
