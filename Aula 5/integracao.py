# -*- coding: utf-8 -*- 

import math #Utilizado apenas para realização de cálculo arcosseno e erf. Utilizadas na integral das funções passadas.

#Autor: Thiago Meyer

'''
Objetivo:
	Realizar a Integral númerica de funções utilizando Simpson, Retângulo e Trapézio.
Resumo:
	Utilizando as regras de Simpson, Retângulo e Trapézio, é desejado aproximar o valor das funções, minimizando o erro de cada função.
	Para testes, será utilizado o método de Quadratura adaptativa para aproximar o valor à um erro mínimo desejado.
'''
def Simpson(funcao,FuncIntegrada,a,b,n=2):
	dx = (b-a)/float(n)
	s1 = 0
	for i in range(1,n/2 +1):
		s1 += funcao(a+(2*i -1)*dx)
	s1 *= 4
	s2 = 0
	for i in range(1,n/2):
		s2 +=funcao(a+2*i*dx)
	s2 *= 2
	Sn = (b-a)/(3.0*n) * (funcao(a)+funcao(b)+s1+s2)
	Error = abs((FuncIntegrada(b) - FuncIntegrada(a))- Sn)
	return (Sn, Error)

def Rectangle(funcao,FuncIntegrada,a,b,n=2):
	total = 0.0
	dx = (b-a)/float(n)
	for i in range(0,n):
		total = total +funcao((a+(i*dx)))

	Error = abs((FuncIntegrada(b) - FuncIntegrada(a))- (dx*total))
	return (dx*total,Error)

def Trapezoidal(funcao,FuncIntegrada,a,b,n=2):
	dx = (b-a)/float(n)
	s = 0.0
	s += funcao(a)/2.0
	for i in range(1,n):
		s += funcao(a+i*dx)
	s += funcao(b)/2.0
	Error = abs((FuncIntegrada(b) - FuncIntegrada(a))- (dx*s))
	return (s * dx,Error)

def quadratura_adap(metodo, funcao, funcaoIntegrada, a,b,Erro_minimo,n=2):
	s, Erro = metodo(funcao,funcaoIntegrada,a,b,n)
	if Erro < Erro_minimo:
		return (s, Erro, n)
	else:
		return quadratura_adap(metodo,funcao,funcaoIntegrada,a,b,Erro_minimo,n=n*2)

def Sf1(x):
	return math.e**x

def f1(x):
	return math.e**x

def Sf2(x):
	return (math.asin(x) + x*(1-x**2)**0.5)/2

def f2(x):
	return (1-x**2)**0.5

def Sf3(x):
	return ((math.pi**0.5)*math.erf(x))/2 

def f3(x):
	return math.e**(-x**2)

Sn1, ErSn1 = Simpson(f1,Sf1,0,1)
Sn2, ErSn2 = Simpson(f2,Sf2,0,1)
Sn3, ErSn3 = Simpson(f3,Sf3,0,1)
Rec1,ErRec1 = Rectangle(f1,Sf1,0,1)
Rec2,ErRec2 = Rectangle(f2,Sf2,0,1)
Rec3,ErRec3 = Rectangle(f3,Sf3,0,1)
Trap1, ErTrap1 = Trapezoidal(f1,Sf1,0,1)
Trap2, ErTrap2 = Trapezoidal(f2,Sf2,0,1)
Trap3, ErTrap3 = Trapezoidal(f3,Sf3,0,1)

print "Função 1 Simpson: "+str(Sn1)
print "Erro Função 1 Simpson: "+str(ErSn1)
print "Função 1 Rectangle: "+str(Rec1)
print "Erro Função 1 Rectangle: "+str(ErRec1)
print "Função 1 Trapezoidal: "+str(Trap1)
print "Erro Função 1 Trapezoidal: "+str(ErTrap1)
print '------------------------------------------'
print "Função 2 Simpson: "+str(Sn2)
print "Erro Função 2 Simpson: "+str(ErSn2)
print "Função 2 Rectangle: "+str(Rec2)
print "Erro Função 2 Rectangle: "+str(ErRec2)
print "Função 2 Trapezoidal: "+str(Trap2)
print "Erro Função 2 Trapezoidal: "+str(ErTrap2)
print '------------------------------------------'
print "Função 3 Simpson: "+str(Sn3)
print "Erro Função 3 Simpson: "+str(ErSn3)
print "Função 3 Rectangle: "+str(Rec3)
print "Erro Função 3 Rectangle: "+str(ErRec3)
print "Função 3 Trapezoidal: "+str(Trap3)
print "Erro Função 3 Trapezoidal: "+str(ErTrap3)

s, Verro, n = quadratura_adap(Simpson,f1,Sf1,0,1,0.000001)
print '------------------------------------------'
print "Utilizando quadratura adaptativa, Simpson: "+str(s)
print "Utilizando quadratura adaptativa, erro de Simpson: "+str(Verro)
print "Utilizando quadratura adaptativa, n cortes: "+str(n)

s, Verro, n = quadratura_adap(Rectangle,f1,Sf1,0,1,0.000001)
print '------------------------------------------'
print "Utilizando quadratura adaptativa, Rectangle: "+str(s)
print "Utilizando quadratura adaptativa, erro de Rectangle: "+str(Verro)
print "Utilizando quadratura adaptativa, n cortes: "+str(n)

s, Verro, n = quadratura_adap(Trapezoidal,f1,Sf1,0,1,0.000001)
print '------------------------------------------'
print "Utilizando quadratura adaptativa, Trapezoidal: "+str(s)
print "Utilizando quadratura adaptativa, erro de Trapezoidal: "+str(Verro)
print "Utilizando quadratura adaptativa, n cortes: "+str(n)