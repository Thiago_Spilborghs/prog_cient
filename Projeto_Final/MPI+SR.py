from mpi4py import MPI
import numpy
import speech_recognition as sr
import jellyfish as jf
from difflib import SequenceMatcher
import datetime
import sys
import os
import time

def Spec_calc(phrase,Spec): #calculo das frases mais provaveis a serem reconhecidas
	Sim_Rate = 0.0
	Near_phrase = ''
	plist = []
	for i in Spec: #frases possiveis e esperadas
		tempVal = jf.jaro_winkler(unicode(phrase),unicode(i)) #calculo de probabilidade e aproximacao utilizando o metodo de jaro-wrinkler.
		if tempVal >= 0.6: #avaliacao com mais de 60% e aceitavel para as frases genericas
			plist.append(i)
	return  plist

def Choices_Calc(spec,phrase,Choices,Percentage=False): #calculo de escolhas possiveis para as frases genericas

	s = spec.split()
	p = phrase.split()
	resp = []
	i = 0
	Sim_Rate = 0.0
	interm = ''
	newPhrase = spec
	for xs in s:
		if xs[0] == '<':
			for N in Choices:
				if N == xs[1:-1]:
					Sim_Rate = 0.0
					for v in Choices[N]:
						if i < len(p):
							if SequenceMatcher(None, p[i], v).ratio() > Sim_Rate: #calculo de probabilidade da escolha
								Sim_Rate = SequenceMatcher(None, p[i], v).ratio() 
								interm = v
						else :
							if SequenceMatcher(None, p[len(p)-1], v).ratio() > Sim_Rate: #calculo de probabilidade da escolha
								Sim_Rate = SequenceMatcher(None, p[len(p)-1], v).ratio()
								interm = v
					x = (N,interm)
					newPhrase = newPhrase.replace(N,interm)
					resp.append(x)
			i = i+1
	if Percentage == True:
		return SequenceMatcher(None, newPhrase, phrase).ratio(), resp #retorna a probabilidade e o vetor com as respostas das escolhas
	else:
		return resp


Choices = {"location":["kitchen","bedroom","bathroom","office"],"object":["coke","box","shampoo","chocolate","milk"],"vbgo":["go","move"],"vblocate":["find","locate"],"vbbring":["bring","fetch"]} 
#Choices sao as opcoes dadas para as frases genericas
Spec = ["<vbgo> to the <location> and <vbbring> me a <object>","<vbgo> to the <location>","<vbbring> the <object> to me","how many <object> are in the <location>","<vbgo> to the <location> and <vblocate> a <object>","<vblocate> and object in the <location>", "<vbbring> me an object from <location>","Where can I find the <object>"]
#Spec e o esperado para reconhecimento, as frases genericas gravadas no vetor

r = sr.Recognizer() #inicio do sistema de reconhecimento
comm = MPI.COMM_WORLD #criacao da comunicacao mpi
nprocs = comm.Get_size() #recuperacao da quantidade de processos
myrank = comm.Get_rank() # recebe  o numero do processo

if myrank == 0:
	audio = None
	with sr.Microphone(sample_rate=16000) as source: #inicia a calibracao par reconhecimento
		r.adjust_for_ambient_noise(source, duration=2) #calibra o microfone para o ambiente
		time.sleep(2) #aguarda 2 segundos para calibracao
		print("Say something!") #informa para reconhecimento
		#audio = r.record(source,duration=7) Record e um metodo onde a duracao e o valor que quantos segundos o computaador deve gravar a voz
		audio = r.record(source,duration=7)
	
	recog = r.recognize_google(audio) #realiza o reconhecimento

else:
	recog = None

recog = comm.bcast(recog,root=0) #realiza o broadcast para os processos do que foi reconhecido

step = len(Spec)/nprocs #passos pra particionar as frases possiveis
resp = None
spec = None
percao = 0.0
chosen = ''
p = Spec_calc(recog,Spec[0+step*myrank:0+(step*myrank)+step]) #calculo das frases
if len(p) == 1:
	resp = Choices_Calc(p[0],recog,Choices) #calculo das escolhas
	spec = p[0]
else:
	for ph in p:
		Perc, Ch = Choices_Calc(ph,recog,Choices,True) #calculo das escolhas
		if Perc > percao:
			percao = Perc
			resp = Ch
			spec = ph

resp = comm.gather(resp, root=0) #retorna as escolhas para o processo raiz
spec = comm.gather(spec,root=0) #retorna as possiveis frases para o processo raiz
# caso seja o processo inicial, inicia a verificacao que qual foi a frase reconhecida com base no esperado
if myrank == 0:
	Resposta_Final = ""
	perc = 0
	for i in spec:
		if i != None:
			nwph = i
			for ans in resp:
				if ans != None:
					for j in ans:
						if j != None:
							ch = "<"+j[0]+">"
							if ch in nwph:
								nwph = nwph.replace(ch,j[1])
			if jf.jaro_winkler(unicode(nwph),unicode(recog)) > perc: #verificacao da maior probabilidade da frase correta
				Resposta_Final = nwph
				perc = jf.jaro_winkler(unicode(nwph),unicode(recog))

	print (Resposta_Final,perc)

